/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

/** Lightweight reference to an immutable string stored elsewhere.
 *
 * The underlying string does not need to be zero-terminated.  The stringlet
 * knows its length as well as its location.
 *
 * A stringlet does not "own" (or deallocate) the string it points to.  The
 * original string must stay in place for as long as this is used.  It must
 * also stay unchanged, because the stringlet caches part of its contents
 * internally.
 *
 * The string's length is kept as an unsigned int, not a std::size_t, so on a
 * system with 64-bit pointers and 32-bit ints, a single string can be no more
 * than 4 GB long.
 */
class stringlet
{
public:
    typedef unsigned int size_type;

    stringlet() noexcept :
        m_data(nullptr),
        m_size(0),
        m_preview(0)
    {}

    explicit stringlet(const char data[], std::size_t len) noexcept :
        m_data(data),
        m_size(len),
        m_preview(
            summarize(reinterpret_cast<const unsigned char *>(data), m_size))
    {
    }

    stringlet(const stringlet &) =default;
    stringlet(stringlet &&) =default;
    stringlet &operator=(const stringlet &) =default;
    stringlet &operator=(stringlet &&) =default;

    std::size_t size() const noexcept { return m_size; }
    const char *data() const noexcept { return m_data; }

    bool operator<(const stringlet &rhs) const noexcept
    {
        if (m_preview != rhs.m_preview) return m_preview < rhs.m_preview;
        const int diff = std::memcmp(
            data(), rhs.data(), std::min(size(), rhs.size()));
        return
            (diff < 0) ||
            ((diff == 0) && (size() < rhs.size()));
    }

    bool operator>(const stringlet &rhs) const noexcept
        { return rhs < *this; }
    bool operator<=(const stringlet &rhs) const noexcept
        { return !(*this > rhs); }
    bool operator>=(const stringlet &rhs) const noexcept
        { return !(*this < rhs); }

    bool operator==(const stringlet &rhs) const noexcept
    {
        return
            m_preview == rhs.m_preview &&
            m_size == rhs.m_size &&
            std::memcmp(m_data, rhs.m_data, std::min(m_size, rhs.m_size)) == 0;
    }

    bool operator!=(const stringlet &rhs) const noexcept
        { return !(rhs == *this); }

    bool empty() const noexcept { return m_size == 0; }
    char front() const noexcept
        { return char(m_preview >> 8 * (sizeof(m_preview) - 1)); }
    char back() const noexcept
        { return m_data[m_size - 1]; }

private:
    static unsigned int summarize(const unsigned char data[], size_type len)
        noexcept
    {
        unsigned int summary = 0;
        len = std::min(len, size_type(sizeof(summary)));
        size_type bytes;
        for (bytes = 0; bytes < len; bytes++)
            summary = (summary << 8) | data[bytes];
        summary <<= 8 * (sizeof(summary) - bytes);
        return summary;
    }

    const char *m_data;
    /// String length.  Keep this short to leave room for m_preview.
    size_type m_size;
    /// Inline copy of the string's initial bytes, in Big-Endian order.
    unsigned int m_preview;
};


std::ostream &operator<<(std::ostream &, stringlet);
