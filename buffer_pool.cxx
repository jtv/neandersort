/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <atomic>
#include <cassert>
#include <string>
#include <vector>

#include <boost/chrono/duration.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "buffer_pool.h"
#include "thread_pool.h"


buffer_pool::buffer_pool(int capacity, bool unique) :
    m_free_buffers(),
    m_buffers(capacity),
    m_buffer_states(capacity, buf_state::free),
    m_alloc_pos(0)
{
}


int buffer_pool::next_buffer(int id, const unique_lock &lock) const noexcept
{
    assert(lock.owns_lock());
    assert(id >= 0);
    return (id + 1) % int(m_buffers.size());
}


int buffer_pool::find_buffer(
    buf_state state, int start, const unique_lock &lock
    ) const noexcept
{
    assert(lock.owns_lock());
    int id = start;
    do
    {
        if (m_buffer_states[id] == state) return id;
        id = next_buffer(id, lock);
    } while (id != start);
    return no_buffer;
}


int buffer_pool::allocate(std::size_t bytes, thread_pool &threads)
{
    while (true)
    {
        unique_lock lock(m_mutex);
        const int id = find_buffer(buf_state::free, m_alloc_pos, lock);
        if (id != no_buffer)
        {
            m_buffer_states[id] = buf_state::filling;
            m_alloc_pos = next_buffer(id, lock);
            m_buffers[id].reserve(bytes);
            return id;
        }

        // No buffer available.  Block and try again.
        m_free_buffers.wait(lock);
        threads.check_error();
    }
}


void buffer_pool::deallocate(int id)
{
    assert(id >= 0);
    lock_guard lock(m_mutex);
    assert(m_buffer_states[id] != buf_state::free);
    m_buffers[id].clear();
    m_buffer_states[id] = buf_state::free;
    m_free_buffers.notify_one();
}


std::vector<char> &buffer_pool::get(int id) noexcept
{
    // Does not need a lock: buffer vectors always stay in place.
    assert(id >= 0);
    return m_buffers[id];
}


const std::vector<char> &buffer_pool::get(int id) const noexcept
{
    // Does not need a lock: buffer vectors always stay in place.
    assert(id >= 0);
    return m_buffers[id];
}


void buffer_pool::queue_full_buffer(int id)
{
    lock_guard lock(m_mutex);
    assert(m_buffer_states[id] == buf_state::filling);
    assert(!m_buffers[id].empty());
    m_buffer_states[id] = buf_state::full;
}


void buffer_pool::wait_none_full(thread_pool &threads) const
{
    do
    {
        unique_lock lock(m_mutex);
        if (find_buffer(buf_state::full, 0, lock) == no_buffer) return;
        m_free_buffers.wait(lock);
        threads.check_error();
    } while (!threads.is_aborting());
}


int buffer_pool::grab_full_buffer()
{
    unique_lock lock(m_mutex);
    const int id = find_buffer(
        buf_state::full, next_buffer(m_alloc_pos, lock), lock);
    if (id != no_buffer)
        m_buffer_states[id] = buf_state::sorting;
    return id;
}


void buffer_pool::abort()
{
    m_free_buffers.notify_all();
}
