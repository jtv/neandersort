#! /usr/bin/make

override CXXFLAGS += -std=c++14 -Wall

# Both boost_thread and boost_system could go now that we have C++14.
override LOADLIBES += \
    -lboost_program_options -lboost_iostreams -lboost_thread -lboost_system \
    -lz -lpthread

PLATFORM ?= Linux

DEBUG ?= 0
PROFILE ?= 0
COVERAGE ?= 0
DYNAMIC ?= 0

ifeq "$(PLATFORM)" "Darwin"
    # MacOS supports dynamic or static linking like everyone else, except for
    # the runtime library.  The OS defines no fixed link-time ABI between
    # userland and the OS, fixing the API entirely in userland.  Or such is my
    # understanding.  At any rate, crt0 must be linked dynamically.
    DYNAMIC := 1
endif

ifeq "$(DEBUG)" "1"
    override CXXFLAGS += -g
endif

ifeq "$(PROFILE)" "1"
    override CXXFLAGS += -pg -fprofile-correction
    override LDFLAGS += -pg -fprofile-correction
    override CPPFLAGS += -DNDEBUG
endif

ifeq "$(COVERAGE)" "1"
    override CXXFLAGS += -fprofile-arcs -ftest-coverage
    override CPPFLAGS += -DNDEBUG
endif

ifeq "$(DEBUG)$(PROFILE)$(COVERAGE)" "000"
    O ?= 3
else
    O ?= 0
endif

ifneq "$(O)" "0"
    OPTIMIZE := -O$(O)
else
    OPTIMIZE :=
endif


ifeq "$(PLATFORM)" "MinGW"
    EXE := .exe
else
    EXE :=
endif

ifneq "$(DYNAMIC)" "1"
    STATIC_LINK := -static
else
    STATIC_LINK :=
endif


BINARY := neandersort$(EXE)

MKDIR := mkdir -p

all: $(BINARY)


unit_test_sources := $(wildcard unit-test/test_*.cxx)
unit_test_modules := $(basename $(unit_test_sources))
unit_test_objs := $(addsuffix .o, $(unit_test_modules))
main_sources := $(wildcard *.cxx)
main_modules := $(basename $(main_sources))
main_objs := $(addsuffix .o, $(main_modules))


clean:
	$(RM) -r test-data/output/
	$(RM) -- *.gcda *.gcno *.gcov gmon.out
	$(RM) -- $(unit_test_objs) $(main_objs)


distclean: clean
	$(RM) neandersort neandersort.exe test_runner
	$(RM) -r test-data/reference/


# Generate the output data that regular "sort" would produce.
reference:
	@$(MKDIR) test-data/reference/exact
	@$(MKDIR) test-data/reference/unique
	@echo "Writing reference outputs."
	@for f in test-data/input/*; do \
	    tr -d '\r' <$$f | LC_ALL=C sort -o test-data/reference/exact/$${f##*/}; \
	    tr -d '\r' <$$f | LC_ALL=C sort -u -o test-data/reference/unique/$${f##*/}; \
	done


# Compare output to reference output from regular "sort" program.
#
# For each test input, generates and compares to the reference output:
#  - A plain, "exact" sort.
#  - A "unique" sort.
#  - A sort from a Windows version of the file, i.e. with CR/LF line endings.
#
acceptance_test: $(BINARY)
	./utilities/acceptance-test.sh

test: acceptance_test unit_test

unit_test: test_runner
	./$<

# Apparently we need -lboost_unit_test_framework or -lboost_test_exec_monitor,
# but not both.
test_runner: $(unit_test_objs) $(filter-out main.o, $(main_objs))
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) $(LOADLIBES) \
	    -lboost_unit_test_framework \
	    -lboost_filesystem -lboost_system \
	    -o $@


unit-test/test_%.o: unit-test/test_%.cxx
	$(CXX) -c $(CPPFLAGS) -I. -DBOOST_TEST_DYN_LINK $(CXXFLAGS) $< -o $@

%.o: %.cxx
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $(OPTIMIZE) $< -o $@

$(BINARY): *.cxx *.h
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OPTIMIZE) *.cxx \
	    $(LDFLAGS) $(STATIC_LINK) $(LOADLIBES) -o $@


.PHONY: acceptance_test all clean distclean reference test unit_test
