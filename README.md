Neandersort
===========

Primitive utility to sort lines of text.

This is a sort utility optimized for 64-bit multi-CPU systems.  It will work on 32-bit single-CPU systmms, but it won't hit its stride performance-wise and it probably won't be able to handle multi-gigabyte files The source code is in C++14.


Features
--------

Neandersort is meant to be simple, even if some of the multiprocessing optimization gets a little complex.  It's meant to support only the few features which are actually required for its primary use-case: the Moses decoder and accompanying utilities.  We want that functionality, and we want speed, and otherwise everything should stay primitive.  The name was chosen to remind us of these goals: think Neanderthal Sort.  Brute force, little sophistication, no time for difficult stuff!

Thus Neandersort does _not_ support, for example:

 - Extremely long lines.  If a line is longer than one buffer (20 MB by default), it just gives up.
 - Wide-character encodings.  Use UTF-8 or anything single-byte.
 - Exotic encodings: a newline must always be '\n'.
 - Locales.  It's always a plain memcmp()-style sort.
 - Windows line endings on output.  Newline is always '\n'.
 - Reversed sorting.
 - Numeric sort, date sort, or field-based sort.

On the other hand, Neandersort _does_ support:

 - Transparent gzip decompression of input, and compression of output.
 - Windows-style ("\r\n") and Unix-style ("\n") line endings on input.
 - Elimination of duplicate lines.


Performance
-----------

At the moment Neandersort seems to be significantly slower than GNU sort for large files on a 32-bit test system with a traditional hard drive.  On the other hand, it's faster than GNU sort for large files on a 64-bit test system with a solid-state drive.

Number of threads or buffer size don't seem to make much of a difference.  For large files, the big question may be whether they will be written to a traditional platter-based hard drive with long seek latencies.  So the one thing that the user may really need to be able to tweak is how many temporary files get written at one time.
