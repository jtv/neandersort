/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "temp_file.h"
#include "threading.h"


class temp_file;


// TODO: Support limiting this.  Platters prefer sequential writes.

/** Pool of temporary files.
 *
 * The files are reusable and persistent.  So, releasing a file and later
 * re-allocating it will preserve its contents.
 */
class temp_file_pool
{
public:
    explicit temp_file_pool(const std::string &temp_dir);

    temp_file_pool(const temp_file_pool &) =delete;
    temp_file_pool &operator=(const temp_file_pool &) =delete;

    int grab();
    void release(int id) noexcept;
    temp_file &get(int id) noexcept;
    const temp_file &get(int id) const noexcept;
    std::size_t size() const noexcept;
    void flush();

private:
    const std::string m_temp_dir;
    mutable threading::mutex m_mutex;
    /// Pointers to temp_file.  Not stored inline to keep them from moving.
    std::vector<std::unique_ptr<temp_file>> m_files;
    /// Which temp files are available for allocation right now?
    std::vector<bool> m_available;
};
