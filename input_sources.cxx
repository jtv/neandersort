/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/iostreams/filtering_stream.hpp>

#include "input_sources.h"


input_sources::input_sources(
    const std::vector<std::string> &filenames,
    bool ungzip
    ) :
    m_current(),
    m_filenames(filenames.rbegin(), filenames.rend()),
    m_ungzip(ungzip)
{
    next();
}


bool input_sources::next()
{
    m_current.close();
    if (m_filenames.empty()) return false;
    m_current.open(m_filenames.back(), m_ungzip);
    m_filenames.pop_back();
    return true;
}
