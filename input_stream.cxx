/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <system_error>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include "except.h"
#include "input_stream.h"
#include "log.h"


input_stream::~input_stream()
{
    try
    {
        close();
    }
    catch (const std::exception &error)
    {
        log_err(error.what());
        log_err_end();
    }
}


void input_stream::close()
{
    m_stream.reset();
    if (m_owned_file.is_open()) m_owned_file.close();
    m_path.clear();
}


void input_stream::open(const std::string &path, bool ungzip)
{
    close();
    m_path = path;
    if (ungzip || boost::algorithm::ends_with(m_path, ".gz"))
        m_stream.push(boost::iostreams::gzip_decompressor());
    if (m_path == "-")
    {
        m_stream.push(std::cin);
    }
    else
    {
        m_owned_file.open(m_path, std::ios_base::in | std::ios_base::binary);
        if (!m_owned_file)
            throw std::system_error(
                errno, std::system_category(),
                "Could not open input file '" + m_path + "'");
        m_stream.push(m_owned_file);
    }
}

std::size_t input_stream::read(char buffer[], std::size_t bytes)
{
    m_stream.read(buffer, bytes);
    if (!m_stream.good() && !m_stream.eof())
        throw std::system_error(
            errno, std::system_category(),
            "Error reading from '" + m_path + "'");
    return m_stream.gcount();
}


bool input_stream::eof() const noexcept
{
    return m_stream.empty() || m_stream.eof();
}
