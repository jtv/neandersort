/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "input_stream.h"


/** Series of input files/streams.
 *
 * Opens, in turn and in the order given, each of the given streams for input
 * and makes them available for use.
 *
 * As a special case, a filename of "-" (a dash) denotes standard input.
 */
class input_sources
{
public:
    explicit input_sources(
        const std::vector<std::string> &filenames, bool ungzip=false);

    input_sources() =delete;
    input_sources(const input_sources &) =delete;
    input_sources(input_sources &&) =delete;
    input_sources &operator=(const input_sources &) =delete;
    input_sources &operator=(input_sources &&) =delete;

    /** Open next input stream.
     *
     * @return Whether there was another input stream to open.
     */
    bool next();
    
    input_stream &get() noexcept
    {
        return m_current;
    }

    /// Name of the current stream.
    std::string name() const noexcept
    {
        return m_current.path();
    }

    /// Are we all out of input data?
    bool empty() const noexcept
    {
        return m_current.eof() && m_filenames.empty();
    }

private:

    /// Current input stream.
    input_stream m_current;
    /// Remaining streams to open.
    std::vector<std::string> m_filenames;
    /// Transparently decompress inputs as gzip?
    bool m_ungzip;
};
