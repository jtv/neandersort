/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stringlet.h"


class temp_file;


// TODO: Split into store-phase class and read-phase class.

/** Chunk of sorted lines, backed by storage in a temporary file.
 *
 * The data gets written to a file sequentially during the "store" phase.
 * Later on, memory-map the entire file and tell the chunk where it's located
 * in memory so it can find its own data back.  At that point the chunk goes
 * into the "read" phase where you read lines of text out of it.
 *
 * Multiple chunks can live in the same file.  This keeps the number of
 * temporary files from getting out of hand.
 */
class chunk
{
public:
    /// Create a chunk, to be appended to the given file.
    explicit chunk(temp_file &file);

    /// Return file where this chunk is stored (along with possibly others).
    const std::string &path() const noexcept;

    /// Write a line during the store phase.
    void store_line(stringlet);

    /// Complete the store phrase.
    void complete_store();

    /// Start the read phase on a chunk that's been mapped into memory.
    void map(boost::iostreams::mapped_file_source &);

    /// Move on to the next line during the read phase.
    void next_line();
    /// Is there still a line to be processed?
    bool has_line() const noexcept { return m_have_line; }
    /// Obtain the current line during the read phrase.
    stringlet line() const noexcept { return m_line; }

private:
    temp_file &m_file;

    /// At what offset into the file does this chunk's data start?
    std::size_t m_offset;

    /// How many bytes in this chunk's file storage?
    std::size_t m_size;

    /// Memory-mapped data location for this chunk.
    const char *m_data;

    /// At what offset in the chunk do we start reading the next line?
    std::size_t m_read_offset;

    /// Next line to process in the read phase.
    stringlet m_line;

    /// Do we have a next line to process?
    bool m_have_line;
};


/// Memory-map chunks.
std::vector<boost::iostreams::mapped_file_source> map_chunks(
    std::vector<std::unique_ptr<chunk>> &chunks);
