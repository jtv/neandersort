/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "threading.h"


/** Special-purpose exception: aborting threads because of some other error.
 *
 * To be thrown only in threads in the pool.
 */
class stop_threads {};


/** Thread pool with fixed capacity.
 *
 * Manages a limited set of persistent threads.  Create threads on the pool
 * until it runs out of capacity.  Each thread, once created, is expected to
 * stay around until the pool is done.  Once the pool is done, wait until the
 * pool is in a state where threads can tell whether there will be any more
 * work for them to do; call request_quiesce() to ask the threads to shut down
 * once all work is done; notify any threads that may be waiting on resources
 * not managed by the pool; and call join() to wait for the threads to
 * complete.
 *
 * If any thread fails, it should call register_error() to propagate the error
 * to the pool's owner.  It will abort the entire thread pool.  Only the first
 * error will be retained; any subsequent ones are assumed to be secondary.
 *
 * Each thread body should also catch stop_threads exceptions, and simply exit.
 *
 * If the owner needs to wait on any resources not managed by the pool, make
 * sure it gets woken up in the event of failure.  Otherwise the whole thing
 * can deadlock.
 */
class thread_pool
{
public:
    explicit thread_pool(int capacity, std::function<void ()> on_error=[](){});

    ~thread_pool();

    thread_pool() = delete;
    thread_pool(const thread_pool &) = delete;
    thread_pool(thread_pool &&) = delete;
    thread_pool &operator=(const thread_pool &) = delete;
    thread_pool &operator=(thread_pool &&) = delete;

    /** Start a new thread, if the pool has a free slot for it.
     *
     * Pass the callable which the thread is supposed to execute, and any
     * arguments that should be passed into it.
     *
     * Returns success: true if a thread was created and set to work, or false
     * if all slots were full.
     */
    template<typename... Args> inline bool allocate(Args &&...args)
    {
        lock_guard lock(m_mutex);
        assert(!m_quiescing);
        if (m_threads.size() == m_threads.capacity())
            // All thread slots are already used up.
            return false;

        m_threads.emplace_back(std::forward<Args>(args)...);
        return true;
    }

    /// Put this thread to sleep until woken up.
    void sleep();

    /** Put this thread to sleep until predicate evaluates to true.
     *
     * If the predicate is true initially, the thread never goes to sleep.
     * Otherwise, it sleeps until it is woken up at a time when the predicate
     * evaluates to true.
     *
     * The predicate is evaluated while holding the object's built-in mutex.
     *
     * Calls check_error.  Therefore, if this is called from the owning thread,
     * it may throw an exception originated in a worker thread.  If called from
     * a thread in the pool, it may throw stop_threads.
     *
     * The on_wakeup callable is invoked after wakeup, while holding the mutex.
     */
    void sleep(
        std::function<bool()> predicate,
        std::function<void()> on_wakeup=[](){});

    /// Wake up one thread to pick up some work.
    void wake_up_thread();

    /// Emergency stop.  Ask all threads to stop as soon as possible.
    void request_abort();

    /// Ask all running threads to complete their work and exit.
    void request_quiesce();

    /** Wait for all running threads to finish.
     *
     * Always call this after completing use of the thread pool.  If use cannot
     * be completed due to an exception, the pool will kill off its threads on
     * destruction.
     */
    void join();

    /** Is the pool shutting down?
     *
     * When this returns true, threads in the pool should finish the available
     * work, and exit.
     */
    bool is_quiescing() const noexcept
        { return m_quiescing.load(std::memory_order_relaxed); }

    /** Is the pool aborting?
     *
     * When this returns true, threads in the pool should exit as soon as they
     * can.
     */
    bool is_aborting() const noexcept
        { return m_abort.load(std::memory_order_relaxed); }

    /** Register a fatal exception from a thread in the pool.
     *
     * Call this from an exception handler.  It captures the current exception.
     *
     * The pool's owner can attempt to report the error.  If multiple errors
     * are registered, only the first will be reported.
     */
    void register_error();

    /** Check for errors on the pool.
     *
     * This does two different things depending on who calls it.
     *
     * If the calling thread is the thread which owns the pool, this throws any
     * error that may have been registered by a failing thread.
     *
     * Otherwise, the calling thread is assumed to be a thread in the pool.  If
     * the thread is aborting, it throws stop_threads.
     */
    void check_error() const;

    int idle_threads() const noexcept;

    std::size_t size() const noexcept;

private:
    void check_error(const unique_lock &) const;

    /// Lock to protect state changes.
    mutable threading::mutex m_mutex;
    /// Pool of threads.  Capacity never changes, size starts at 0.
    std::vector<threading::thread> m_threads;
    /// Waiting point for idle threads.
    mutable threading::condition_variable m_wake_up;
    /// Is this thread pool in the process of shutting down?
    std::atomic<bool> m_quiescing;
    /// Is this thread pool performing an emergency shutdown?
    std::atomic<bool> m_abort;
    /// Error being thrown from thread.
    std::exception_ptr m_error;
    /// Function to call when an error is registered.
    std::function<void()> m_on_error;
    /// The thread that owns this pool.
    threading::thread::id m_owner;
    /// How many threads are sleeping at the moment?
    int m_idle;
};
