/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <system_error>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include "except.h"
#include "output_stream.h"
#include "stringlet.h"


output_stream::output_stream(const std::string &path, bool gzip) :
    m_stream(),
    m_path(path),
    m_owned_file()
{
    if (gzip || boost::algorithm::ends_with(m_path, ".gz"))
        m_stream.push(boost::iostreams::gzip_compressor());
    if (m_path == "-")
    {
        m_stream.push(std::cout);
    }
    else
    {
        m_owned_file.open(m_path, std::ios_base::out | std::ios_base::binary);
        if (!m_owned_file)
            throw std::system_error(
                errno, std::system_category(),
                "Could not open output file '" + m_path + "'");
        m_stream.push(m_owned_file);
    }
}


void output_stream::write_line(stringlet line)
{
    m_stream << line << '\n';
}


void output_stream::close()
{
    if (!m_stream.flush())
        throw std::system_error(
            errno, std::system_category(),
            "Error while flushing output file '" + path() + "'");
    boost::iostreams::close(m_stream);
    if (m_owned_file) m_owned_file.close();
}
