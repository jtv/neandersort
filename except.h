/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

/// Problem with the command line as given.
class command_line_error: public std::domain_error
{
public:
    explicit command_line_error(const std::string &err) :
        std::domain_error(err)
    {}
};


/// Problem with the data; we can't sort.
class data_error: public std::runtime_error
{
public:
    explicit data_error(const std::string &err) : std::runtime_error(err) {}
};
