/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <atomic>
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "lines_buffer.h"
#include "stringlet.h"
#include "thread_pool.h"


lines_buffer::lines_buffer(thread_pool &threads) :
    m_threads(threads),
    m_lines()
{
}


void lines_buffer::push(std::vector<stringlet> &&lines)
{
    assert(!m_threads.is_quiescing());
    if (lines.empty()) return;
    m_threads.sleep(
        [this]() { return this->m_lines.empty(); },
        [this, &lines]() { this->m_lines.swap(lines); });
    m_threads.wake_up_thread();
}


std::vector<stringlet> lines_buffer::pop()
{
    std::vector<stringlet> lines;
    m_threads.sleep(
        [this]()
        {
            return !this->m_lines.empty() || this->m_threads.is_quiescing();
        },
        [this, &lines]()
        {
            this->m_lines.swap(lines);
        });
    m_threads.wake_up_thread();
    return std::move(lines);
}
