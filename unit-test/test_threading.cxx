#include <boost/test/unit_test.hpp>
#include <boost/thread/thread.hpp>

#include "threading.h"


BOOST_AUTO_TEST_CASE(num_cpus_returns_plausible_number)
{
    const int cpus = num_cpus();
    BOOST_CHECK_GT(cpus, 0);
    // Arbitrary limit on how many CPUs a system can plausible have.
    // This may need updating at some point.  Oh, I hope it will.
    BOOST_CHECK_LT(cpus, 10000);
}
