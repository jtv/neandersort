#include <boost/test/unit_test.hpp>

#include "stringlet.h"


BOOST_AUTO_TEST_CASE(has_no_data_by_default)
{
    const stringlet str;
    BOOST_CHECK(str.data() == nullptr);
    BOOST_CHECK_EQUAL(str.size(), 0);
}


BOOST_AUTO_TEST_CASE(adopts_contents_and_length)
{
    const std::string original = "Test string";
    const stringlet str(original.c_str(), original.size());
    BOOST_CHECK_EQUAL(str.data(), original.c_str());
    BOOST_CHECK_EQUAL(str.size(), original.size());
}


namespace
{
/// Test all comparison operators for two equal stringlets.
void test_equal(const stringlet &lhs, const stringlet &rhs)
{
    BOOST_CHECK(!(lhs < rhs));
    BOOST_CHECK(lhs <= rhs);
    BOOST_CHECK(!(lhs > rhs));
    BOOST_CHECK(lhs >= rhs);
    BOOST_CHECK(lhs == rhs);
    BOOST_CHECK(!(lhs != rhs));
}


/// Test all comparison operators for unequal stringlets: lesser < greater.
void test_less(const stringlet &lesser, const stringlet &greater)
{
    BOOST_CHECK(lesser < greater);
    BOOST_CHECK(lesser <= greater);
    BOOST_CHECK(!(lesser > greater));
    BOOST_CHECK(!(lesser >= greater));
    BOOST_CHECK(!(lesser == greater));
    BOOST_CHECK(lesser != greater);
}
} // namespace


BOOST_AUTO_TEST_CASE(blank_equals_blank_equals_empty_equals_empty)
{
    const stringlet blank, empty("", 0);
    test_equal(blank, blank);
    test_equal(blank, empty);
    test_equal(empty, blank);
    test_equal(empty, empty);
}


BOOST_AUTO_TEST_CASE(lowest_nonempty_equals_lowest_nonempty)
{
    const std::string low("\0", 1);
    const stringlet
        low_char("\0", 1),
        low_str(low.c_str(), low.size());
    test_equal(low_char, low_char);
    test_equal(low_char, low_str);
    test_equal(low_str, low_char);
    test_equal(low_str, low_str);
}


BOOST_AUTO_TEST_CASE(blank_and_empty_less_than_lowest_nonempty)
{
    const stringlet blank, empty("", 0), low("\0", 1);
    test_less(blank, low);
    test_less(empty, low);
}


BOOST_AUTO_TEST_CASE(lower_short_nonempty_less_than_higher_short_nonempty)
{
    const stringlet low("\0", 1), a("a", 1), b("b", 1);
    test_less(low, a);
    test_less(low, b);
    test_less(a, b);
    test_equal(a, a);
    test_equal(b, b);
}


BOOST_AUTO_TEST_CASE(short_prefix_less_than_short_superset)
{
    const stringlet prefix("a", 1), longer("a\0", 2), higher("ab", 2);
    test_less(prefix, longer);
    test_equal(longer, longer);
    test_less(prefix, higher);
    test_less(longer, higher);
    test_equal(higher, higher);
}


BOOST_AUTO_TEST_CASE(short_comparison_compares_in_right_order)
{
    const stringlet ab("ab", 2), ba("ba", 2);
    test_less(ab, ba);
}


BOOST_AUTO_TEST_CASE(short_comparison_aligns_left_not_right)
{
    const stringlet lesser("1234", 4), greater("2", 1);
    test_less(lesser, greater);
}


BOOST_AUTO_TEST_CASE(short_comparison_compares_beginning_not_end)
{
    const stringlet lesser("00001", 5), greater("20000", 5);
    test_less(lesser, greater);
}


// The stringlet contains a 4-byte "preview."  We can't observe it directly,
// but implementation-wise everything gets a little different from that point
// on.  All these comparisons have to work not just for short strings, but for
// longer ones as well... and between long and short ones.

BOOST_AUTO_TEST_CASE(long_stringlet_equal_to_self)
{
    const std::string longish("abcde");
    const stringlet
        long_char("abcde", 5),
        long_str(longish.c_str(), longish.size());
    test_equal(long_char, long_char);
    test_equal(long_char, long_str);
    test_equal(long_str, long_char);
    test_equal(long_str, long_str);
}


BOOST_AUTO_TEST_CASE(short_prefix_less_than_long_superset)
{
    const stringlet prefix("abcd", 4), longer("abcd\0", 5), higher("abcde", 5);
    test_less(prefix, longer);
    test_equal(longer, longer);
    test_less(prefix, higher);
    test_less(longer, higher);
    test_equal(higher, higher);
}


BOOST_AUTO_TEST_CASE(long_prefix_less_than_long_superset)
{
    const stringlet
        prefix("abcde", 5), longer("abcde\0", 6), higher("abcdef", 6);
    test_less(prefix, longer);
    test_equal(longer, longer);
    test_less(prefix, higher);
    test_less(longer, higher);
    test_equal(higher, higher);
}


BOOST_AUTO_TEST_CASE(lower_long_nonempty_less_than_higher_long_nonempty)
{
    const stringlet low("\0\0\0\0\0", 5), a("a\0\0\0\0", 5), b("b\0\0\0\0", 5);
    test_less(low, a);
    test_less(low, b);
    test_less(a, b);
    test_equal(a, a);
    test_equal(b, b);
}


BOOST_AUTO_TEST_CASE(long_comparison_considers_bytes_outside_preview)
{
    const stringlet low("abcd1", 5), high("abcd2", 5);
    test_equal(low, low);
    test_less(low, high);
    test_equal(high, high);
}


BOOST_AUTO_TEST_CASE(long_comparison_compares_in_right_order)
{
    const stringlet low("abcd12", 5), high("abcd21", 5);
    test_equal(low, low);
    test_less(low, high);
    test_equal(high, high);
}


BOOST_AUTO_TEST_CASE(comparison_is_unsigned)
{
    test_less(stringlet("\0", 1), stringlet("\xff", 1));
    test_less(stringlet("abcd\0", 5), stringlet("abcd\xff", 5));
}


BOOST_AUTO_TEST_CASE(comparison_ignores_following_characters)
{
    test_equal(stringlet("a1", 1), stringlet("a2", 1));
    test_equal(stringlet("abcde1", 5), stringlet("abcde2", 5));
}


BOOST_AUTO_TEST_CASE(comparison_does_not_stop_at_null_byte)
{
    test_less(stringlet("a\0b", 3), stringlet("a\0c", 3));
    test_less(stringlet("abcd\0e", 6), stringlet("abcd\0f", 6));
}


BOOST_AUTO_TEST_CASE(copy_constructor_makes_identical_copy)
{
    const stringlet inputs[] = {
        stringlet(),
        stringlet("", 0),
        stringlet("\0", 1),
        stringlet("aa", 2),
        stringlet("12345", 5),
        };
    for (auto &input: inputs) {
        stringlet copy(input);
        test_equal(copy, copy);
        test_equal(input, copy);
        test_equal(copy, input);
    }
}


BOOST_AUTO_TEST_CASE(copy_constructor_introduces_no_false_short_equality)
{
    const stringlet lesser("a", 1), greater("b", 1);
    test_less(lesser, greater);
    test_less(lesser, stringlet(greater));
    test_less(stringlet(lesser), greater);
    test_less(stringlet(lesser), stringlet(greater));
}


BOOST_AUTO_TEST_CASE(copy_constructor_introduces_no_false_long_equality)
{
    const stringlet lesser("abcd1", 5), greater("abcd2", 5);
    test_less(lesser, greater);
    test_less(lesser, stringlet(greater));
    test_less(stringlet(lesser), greater);
    test_less(stringlet(lesser), stringlet(greater));
}


BOOST_AUTO_TEST_CASE(copy_assignment_makes_identical_copy)
{
    const stringlet inputs[] = {
        stringlet(),
        stringlet("", 0),
        stringlet("\0", 1),
        stringlet("aa", 2),
        stringlet("12345", 5),
        };
    for (auto &input: inputs) {
        stringlet copy;
        copy = input;
        test_equal(copy, copy);
        test_equal(input, copy);
        test_equal(copy, input);
    }
}


BOOST_AUTO_TEST_CASE(copy_assignment_introduces_no_false_short_equality)
{
    const stringlet lesser("a", 1), greater("b", 1);
    stringlet copy_lesser, copy_greater;
    copy_lesser = lesser;
    copy_greater = greater;
    test_less(lesser, greater);
    test_less(lesser, copy_greater);
    test_less(copy_lesser, greater);
    test_less(copy_lesser, copy_greater);
}


BOOST_AUTO_TEST_CASE(copy_assignment_introduces_no_false_long_equality)
{
    const stringlet lesser("abcd1", 5), greater("abcd2", 5);
    stringlet copy_lesser, copy_greater;
    copy_lesser = lesser;
    copy_greater = greater;
    test_less(lesser, greater);
    test_less(lesser, copy_greater);
    test_less(copy_lesser, greater);
    test_less(copy_lesser, copy_greater);
}


BOOST_AUTO_TEST_CASE(emptiness)
{
    BOOST_CHECK(stringlet().empty());
    BOOST_CHECK(stringlet("", 0).empty());

    BOOST_CHECK(!stringlet("\0", 1).empty());
    BOOST_CHECK(!stringlet("\0\0\0\0\0", 5).empty());
    BOOST_CHECK(!stringlet("abcde", 5).empty());
}


BOOST_AUTO_TEST_CASE(front)
{
    BOOST_CHECK_EQUAL(stringlet("ab", 2).front(), 'a');
    BOOST_CHECK_EQUAL(stringlet("abcde", 5).front(), 'a');
}


BOOST_AUTO_TEST_CASE(back)
{
    BOOST_CHECK_EQUAL(stringlet("ab", 2).back(), 'b');
    BOOST_CHECK_EQUAL(stringlet("abcde", 5).back(), 'e');
}


BOOST_AUTO_TEST_CASE(inserts_into_stream)
{
    const std::string text = "Sample line.";
    const stringlet strlet(text.c_str(), text.size());
    std::stringstream output;
    output << strlet;
    BOOST_CHECK_EQUAL(output.str(), text);
}


BOOST_AUTO_TEST_CASE(insert_ignores_following_characters)
{
    const stringlet strlet("abcde", 3);
    std::stringstream output;
    output << strlet;
    BOOST_CHECK_EQUAL(output.str(), "abc");
}
