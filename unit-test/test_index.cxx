#include <cstring>
#include <memory>
#include <vector>

#include <boost/test/unit_test.hpp>

#include "except.h"
#include "index.h"
#include "stringlet.h"


namespace
{
std::vector<char> make_text_vector(const std::string &text)
{
    std::vector<char> vector(text.size());
    memcpy(&vector[0], text.c_str(), text.size());
    return std::move(vector);
}
} // namespace


BOOST_AUTO_TEST_CASE(find_last_newline_returns_newline)
{
    const auto vector = make_text_vector("a\nb");
    const auto newline = find_last_newline(vector);
    BOOST_CHECK_EQUAL(*newline, '\n');
    BOOST_CHECK(newline == vector.begin() + 1);
}


BOOST_AUTO_TEST_CASE(find_last_newline_returns_last_newline)
{
    const auto vector = make_text_vector("a\nb\nc");
    const auto newline = find_last_newline(vector);
    BOOST_CHECK_EQUAL(*newline, '\n');
    BOOST_CHECK(newline == vector.begin() + 3);
}


BOOST_AUTO_TEST_CASE(find_last_newline_returns_newline_in_last_pos)
{
    const auto vector = make_text_vector("a\nb\n");
    const auto newline = find_last_newline(vector);
    BOOST_CHECK_EQUAL(*newline, '\n');
    BOOST_CHECK(newline == vector.begin() + 3);
}


BOOST_AUTO_TEST_CASE(find_last_newline_returns_newline_in_first_pos)
{
    const auto vector = make_text_vector("\nx");
    const auto newline = find_last_newline(vector);
    BOOST_CHECK_EQUAL(*newline, '\n');
    BOOST_CHECK(newline == vector.begin());
}


BOOST_AUTO_TEST_CASE(find_last_newline_returns_newline_in_only_pos)
{
    const auto vector = make_text_vector("\n");
    const auto newline = find_last_newline(vector);
    BOOST_CHECK_EQUAL(*newline, '\n');
    BOOST_CHECK(newline == vector.begin());
}


BOOST_AUTO_TEST_CASE(find_last_newline_throws_data_error_if_no_newline_found)
{
    const auto vector = make_text_vector("abc");
    BOOST_CHECK_THROW(find_last_newline(vector), data_error);
}


BOOST_AUTO_TEST_CASE(find_last_newline_throws_data_error_if_buffer_empty)
{
    const auto vector = make_text_vector("");
    BOOST_CHECK_THROW(find_last_newline(vector), data_error);
}


BOOST_AUTO_TEST_CASE(index_buffer_indexes_line)
{
    const std::string line = "text line";
    const auto vector = make_text_vector(line + "\n");
    const auto index = index_buffer(vector);
    BOOST_CHECK_EQUAL(index.size(), 1);
    BOOST_CHECK(index[0] == stringlet(line.c_str(), line.size()));
}


BOOST_AUTO_TEST_CASE(index_buffer_indexes_multiple_lines)
{
    const std::string
        line1 = "text line 1",
        line2 = "and line 2";
    const auto vector = make_text_vector(line1 + "\n" + line2 + "\n");
    const auto index = index_buffer(vector);
    BOOST_CHECK_EQUAL(index.size(), 2);
    BOOST_CHECK(index[0] == stringlet(line1.c_str(), line1.size()));
    BOOST_CHECK(index[1] == stringlet(line2.c_str(), line2.size()));
}


BOOST_AUTO_TEST_CASE(index_buffer_ignores_incomplete_trailing_line)
{
    const std::string line = "complete line";
    const auto vector = make_text_vector(line + "\n" "incomplete line");
    const auto index = index_buffer(vector);
    BOOST_CHECK_EQUAL(index.size(), 1);
    BOOST_CHECK(index[0] == stringlet(line.c_str(), line.size()));
}


BOOST_AUTO_TEST_CASE(index_buffer_throws_data_error_if_no_newline)
{
    const auto vector = make_text_vector("incomplete line");
    BOOST_CHECK_THROW(index_buffer(vector), data_error);
}
