#include <ios>

#include <boost/test/unit_test.hpp>

#include "log.h"


BOOST_AUTO_TEST_CASE(log_err_accepts_string)
{
    log_err("(Expected error output.)");
}


BOOST_AUTO_TEST_CASE(log_err_accepts_int)
{
    log_err(12);
}


namespace
{
struct sabotaged_cerr
{
    sabotaged_cerr() : saved_iostate(std::cerr.exceptions())
    {
        // Make cerr blow up on first error.
        // Unfortunately we can't actually inject an error without breaking
        // the entire test runner.
        std::cerr.exceptions(std::ios_base::badbit | std::ios_base::failbit);
    }

    ~sabotaged_cerr()
    {
        std::cerr.exceptions(saved_iostate);
    }

    const std::ios_base::iostate saved_iostate;
};
} // namespace

BOOST_AUTO_TEST_CASE(log_err_does_not_throw)
{
    sabotaged_cerr sabot;
    log_err("(Expected error output.)");
}


BOOST_AUTO_TEST_CASE(log_err_end_does_not_throw)
{
    sabotaged_cerr sabot;
    log_err_end();
}
