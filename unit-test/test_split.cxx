#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/test/unit_test.hpp>

#include "chunk.h"
#include "split.h"


BOOST_AUTO_TEST_CASE(returns_no_text_for_empty_input)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(extracts_single_line)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(extracts_consecutive_lines)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(sorts_lines)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(reads_from_consecutive_files)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(adds_newline_to_file_without_trailing_newline)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(strips_carriage_return)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(fails_if_line_is_as_long_as_bufsize)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(accepts_line_shorter_than_bufsize)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(strips_duplicate_lines_from_chunk_if_unique_requested)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(leaves_duplicate_lines_if_unique_not_requested)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(splits_input_into_chunks)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(strips_carriage_return_just_before_buffer_boundary)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(strips_carriage_return_just_after_buffer_boundary)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(reconstructs_split_line_across_buffer_boundary)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}


BOOST_AUTO_TEST_CASE(reconstructs_split_line_if_newline_in_new_buffer)
{
    BOOST_CHECK(false); // TODO: TEST THIS
}
