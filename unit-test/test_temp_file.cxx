#include <fstream>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem.hpp>
#include <boost/test/unit_test.hpp>

#include "stringlet.h"
#include "temp_file.h"


BOOST_AUTO_TEST_CASE(creates_and_deletes_file)
{
    const std::string temp_dir = get_default_tmp();
    std::string path;
    {
        temp_file file(temp_dir);
        path = file.path();
        BOOST_CHECK(boost::filesystem::is_regular_file(path));
        BOOST_CHECK_EQUAL(boost::filesystem::file_size(path), 0);
    }
    BOOST_CHECK(!boost::filesystem::exists(path));
}


BOOST_AUTO_TEST_CASE(uses_given_temp_directory)
{
    const std::string temp_dir = get_default_tmp();
    temp_file file(temp_dir);
    BOOST_CHECK(boost::algorithm::starts_with(file.path(), temp_dir));
}


BOOST_AUTO_TEST_CASE(write_line_writes_string_plus_line_feed)
{
    const std::string line = "text line";
    temp_file file(get_default_tmp());
    file.write_line(stringlet(line.c_str(), line.size()));
    file.close_output();

    std::ifstream stream(file.path(), std::ios_base::binary);
    std::stringstream read;
    read << stream.rdbuf();
    BOOST_CHECK_EQUAL(read.str(), line + '\n');
}


BOOST_AUTO_TEST_CASE(write_line_appends)
{
    temp_file file(get_default_tmp());
    file.write_line(stringlet("line1", std::strlen("line1")));
    file.write_line(stringlet("line2", std::strlen("line2")));
    file.close_output();

    std::ifstream stream(file.path());
    std::string read;
    stream >> read;
    BOOST_CHECK_EQUAL(read, "line1");
    stream >> read;
    BOOST_CHECK_EQUAL(read, "line2");
}


BOOST_AUTO_TEST_CASE(tell_returns_zero_initially)
{
    temp_file file(get_default_tmp());
    BOOST_CHECK_EQUAL(file.tell(), 0);
}


BOOST_AUTO_TEST_CASE(tell_progresses)
{
    temp_file file(get_default_tmp());
    const std::string line = "Test line.";
    file.write_line(stringlet(line.c_str(), line.size()));
    BOOST_CHECK_EQUAL(file.tell(), line.size() + 1);
}


BOOST_AUTO_TEST_CASE(get_default_tmp_returns_dir)
{
    BOOST_CHECK(boost::filesystem::is_directory(get_default_tmp()));
}


BOOST_AUTO_TEST_CASE(get_default_tmp_is_consistent)
{
    BOOST_CHECK_EQUAL(get_default_tmp(), get_default_tmp());
}
