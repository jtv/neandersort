/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "except.h"
#include "index.h"
#include "stringlet.h"


std::vector<char>::const_iterator find_last_newline(
    const std::vector<char> &buffer)
{
    const auto found = std::find(buffer.rbegin(), buffer.rend(), '\n');
    if (found == buffer.rend())
        throw data_error("Input line exceeds buffer size.");
    return (found.base() - 1);
}


namespace
{
/// Find newline starting at begin.  Assume there is one.
template<typename IT> IT find_assumed_newline(IT begin)
{
    while (*begin != '\n') ++begin;
    return begin;
}
} // namespace


std::vector<stringlet> index_buffer(const std::vector<char> &buffer)
{
    assert(!buffer.empty());
    std::vector<stringlet> lines;
    // Wild stab at how much storage the index may need.  Jumpstart growth.
    lines.reserve(buffer.size() / 20);

    // Use the last newline in the buffer as a sentinel, so the inner search
    // loop doesn't need to check its range.
    const auto end = find_last_newline(buffer) + 1;
    assert(end <= buffer.end());
    auto begin = buffer.begin();
    assert(begin < end);
    do
    {
        assert(begin == buffer.begin() || *(begin - 1) == '\n');
        const auto newline = find_assumed_newline(begin);
// TODO: Sentinel byte at beginning of buffer to speed up this condition?
        const bool saw_cr = (newline > begin && *(newline - 1) == '\r');
        lines.emplace_back(&*begin, newline - begin - int(saw_cr));
        begin = newline + 1;
    }
    while (begin < end);
    return std::move(lines);
}
