#! /bin/sh

set -e


fail() {
    echo $* >&2
    exit 1
}


if ! [ -d test-data ]
then
    fail "Did not find test-data in the current directory."
fi


if ! [ -x neandersort ]
then
    fail "Did not find neandersort executable."
fi

for output_dir in exact unique crlf
do
    mkdir -p test-data/output/$output_dir
done

for input in test-data/input/*
do
    filename=${input##*/}
    ./neandersort -S0.01 $input -o test-data/output/exact/$filename
    ./neandersort -S0.01 -u $input -o test-data/output/unique/$filename
    tr -d '\r' <$input |
        sed -e 's/$/\r/' |
        ./neandersort -S0.01 - -o test-data/output/crlf/$filename
done


diff -r test-data/reference/exact test-data/output/exact
diff -r test-data/reference/unique test-data/output/unique
diff -r test-data/reference/exact test-data/output/crlf
