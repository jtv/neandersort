/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

class stringlet;


/** Output stream.
 *
 * Abstracts away the difference between a file (which we open and close) and
 * standard output (which we should not).
 *
 * Tragically implemented using standard C functions due to a mysterious
 * std::ofstream destructor failure on Windows.
 */
class output_stream
{
public:
    /** Open output stream.
     *
     * @param filename Path to the file to be opened for writing or, as a
     *  special case, "-" (a dash) for standard output.
     * @param gzip Compress output using gzip, even if name does not end in
     *  ".gz".  By default, it depends completely on the filename.
     */
    explicit output_stream(const std::string &path, bool gzip=false);

    output_stream() =delete;
    output_stream(const output_stream &) =delete;
    output_stream(output_stream &&) =delete;
    output_stream &operator=(output_stream &&) =delete;
    output_stream &operator=(const output_stream &) =delete;

    /// Write a line to output.
    void write_line(stringlet line);

    /** Flush, check for errors, and close.
     *
     * Call this before destruction.
     */
    void close();

    const std::string &path() const noexcept { return m_path; }

private:
    boost::iostreams::filtering_ostream m_stream;
    std::string m_path;
    /// Owning handle on output file, if any.  Only used for cleanup.
    std::ofstream m_owned_file;
};
