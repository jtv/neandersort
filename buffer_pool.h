/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "threading.h"

class thread_pool;


/** Buffer pool used for the "input" stage of the algorithm.
 *
 * Holds a fixed number of buffers.  Each buffer is a vector of char, with a
 * fixed capacity (the allocated memory) but a variable size (actual bytes of
 * data).
 */
class buffer_pool
{
    // TODO: Use move semantics for buffer allocation?
public:
    /// Special "null" buffer identifier.
    enum { no_buffer = -1 };

    /// State of a buffer.
    enum buf_state
    {
        /// Buffer is available for filling.  Owned by the pool.
        free,
        /// Buffer is being filled.  Owned by the input thread.
        filling,
        /// Buffer is full, and waiting to be sorted.  Owned by the pool.
        full,
        /// Buffer is being sorted.  Owned by a worker thread.
        sorting,
    };

    buffer_pool(int capacity, bool unique);

    buffer_pool(const buffer_pool &) =delete;
    buffer_pool &operator=(const buffer_pool &) =delete;

    /** Allocate a free buffer, for the purpose of filling it with input data.
     *
     * Returns no_buffer if the thread pool aborts while you're waiting for
     * a free buffer.
     */
    int allocate(std::size_t bytes, thread_pool &);

    /// Free up a buffer.
    void deallocate(int id);

    /// Queue a full buffer for sorting.
    void queue_full_buffer(int id);

    /// Obtain access to a buffer.
    std::vector<char> &get(int buf_id) noexcept;

    /// Obtain access to a buffer.
    const std::vector<char> &get(int buf_id) const noexcept;

    /// Wait until there are no more full buffers.
    void wait_none_full(thread_pool &) const;

    /** Attempt to grab a full buffer for sorting.
     *
     * Returns no_buffer if no full buffers were available.
     */
    int grab_full_buffer();

    /// Abort.  Wake up and fail any pending allocation attempts.
    void abort();

private:
    /** Find buffer in the given state, or return no_buffer.
     *
     * Starts the search at "start", but checks all buffers before giving up.
     */
    int find_buffer(
        buf_state state, int start, const unique_lock &lock
        ) const noexcept;


    /// Get next buffer ID after the given one.  Caller must hold lock.
    int next_buffer(int id, const unique_lock &lock) const noexcept;

    mutable threading::mutex m_mutex;

    /// Condition variable: "buffers are available for use."
    mutable threading::condition_variable m_free_buffers;

    /** Buffers.
     *
     * Does not resize during the pool's lifetime, so the vectors it contains
     * always stay in place.
     */
    std::vector<std::vector<char>> m_buffers;

    /// Buffer states.
    std::vector<buf_state> m_buffer_states;

    /** Next buffer to attempt to allocate.
     *
     * Trying these in round-robin fashion from the last-allocated one on
     * makes it more likely that we'll hit the first free buffer right away.
     */
    int m_alloc_pos;
};
