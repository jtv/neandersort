/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "threading.h"


class stringlet;
class thread_pool;


/** Buffer for handing batches of lines from a producer to a consumer.
 *
 * Essential assumptions:
 *  - The producer also owns, and ultimately cleans up, the thread pool.
 *  - There is only one producer and only one consumer.
 *
 * Breaking either of these assumptions will lead to race conditions.
 */
class lines_buffer
{
public:
    explicit lines_buffer(thread_pool &);

    /** Add some lines into the queue.
     *
     * Call this from the "producer" thread.  It will block until there is room
     * for the new lines.
     */
    void push(std::vector<stringlet> &&lines);

    /** Take any lines off the queue.
     *
     * Call this from the "consumer" thread.  It will block until there are
     * lines on the queue.
     */
    std::vector<stringlet> pop();

private:
    thread_pool &m_threads;
    std::vector<stringlet> m_lines;
};
