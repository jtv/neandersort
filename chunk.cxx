/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <cstring>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>

#include <boost/iostreams/device/mapped_file.hpp>

#include "chunk.h"
#include "except.h"
#include "temp_file.h"


namespace
{
/// Look for a line of text starting at "begin".
stringlet scan_line(const char *begin, const char *end)
{
    assert(begin <= end);
    const auto newline = std::find(begin, end, '\n');
    assert(newline >= begin);
    assert(begin == end || newline < end);
    const auto line_len = newline - begin;
    return stringlet(begin, line_len);
}
} // namespace


chunk::chunk(temp_file &file) :
    m_file(file),
    m_offset(0),
    m_size(0),
    m_data(nullptr),
    m_read_offset(0),
    m_line(),
    m_have_line(false)
{
    m_offset = m_file.tell();
}


const std::string &chunk::path() const noexcept
{
    return m_file.path();
}


void chunk::store_line(stringlet line)
{
    m_file.write_line(line);
}


void chunk::complete_store()
{
    m_size = m_file.tell() - m_offset;
}


void chunk::map(boost::iostreams::mapped_file_source &mapping)
{
    assert(m_offset + m_size <= mapping.size());
    m_data = mapping.data() + m_offset;

    m_have_line = true;
    next_line();
}


void chunk::next_line()
{
    if (m_read_offset == m_size)
    {
        m_have_line = false;
    }
    else
    {
        m_line = scan_line(m_data + m_read_offset, m_data + m_size);
        m_read_offset += m_line.size() + 1;
    }
}


std::vector<boost::iostreams::mapped_file_source> map_chunks(
    std::vector<std::unique_ptr<chunk>> &chunks)
{
    std::vector<boost::iostreams::mapped_file_source> mappings;
    std::vector<std::string> paths;

    for (auto &chnk: chunks)
    {
        const std::string path = chnk->path();
        auto existing = std::find(paths.begin(), paths.end(), path);
        const std::size_t idx = existing - paths.begin();
        if (existing == paths.end())
        {
            paths.emplace_back(path);
            mappings.emplace_back(path);
        }
        assert(idx <= paths.size());
        assert(mappings.size() == paths.size());
        assert(paths[idx] == path);
        chnk->map(mappings[idx]);
    }
    return std::move(mappings);
}
