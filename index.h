/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

class stringlet;


/// Find last newline in buffer.  Throw data_error if not found.
std::vector<char>::const_iterator find_last_newline(const std::vector<char> &);


/** Index the complete lines of text in a buffer.
 *
 * The buffer must be nonempty.
 *
 * If the buffer ends in an incomplete line not terminated with a line feed,
 * this function will ignore that line.  If the buffer contains text without a
 * single line feed, a data_error is thrown.
 *
 * The returned lines do not include the new-line character at the end of each
 * line, nor a preceding carriage-return character if there is one.  Those fall
 * into "cracks" between the indexed lines.
 */
std::vector<stringlet> index_buffer(const std::vector<char> &);
