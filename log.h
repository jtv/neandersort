/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

/// Attempt to log to std::cerr, but don't fail if it doesn't work.
template<typename T> inline void log_err(const T &msg) noexcept
{
    try { std::cerr << msg; }
    catch (const std::exception &) {}
}


/// Conclude error message to std::cerr.
inline void log_err_end() noexcept
{
    try { std::cerr << std::endl; }
    catch (const std::exception &) {}
}
