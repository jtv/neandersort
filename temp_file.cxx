/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include <system_error>

#if !defined(_WIN32) || defined(__MINGW32__)
#include <unistd.h>
#endif

#if defined(_WIN32)
#include <windows.h>
#endif

#include "except.h"
#include "log.h"
#include "stringlet.h"
#include "temp_file.h"


temp_file::temp_file(const std::string &temp_dir)
{
#if defined(_WIN32)
    char name_buf[MAX_PATH];
    if (GetTempFileName(temp_dir.c_str(), "neandersort", 0, name_buf) == 0)
        // TODO: Get more error information from GetLastError().
        throw std::system_error(
            errno, std::system_category(),
            "Could not open temporary file in '" + temp_dir + "'");
    m_path = name_buf;
#else
    std::string name_template = temp_dir + "/neandersort.XXXXXX";
    char name_buf[1'000];
    std::strcpy(name_buf, name_template.c_str());
    if (mkstemp(name_buf) == -1)
        throw std::system_error(
            errno, std::system_category(),
            "Could not create temporary file in '" + temp_dir + "'");
    m_path = std::string(name_buf);
#endif
    m_stream.open(m_path, std::ios_base::out | std::ios_base::binary);
}


temp_file::~temp_file()
{
    if (unlink(m_path.c_str()) == -1)
    {
// TODO: Is there a simple, portable, thread-safe, robust way to perror()?
        log_err("Could not delete temporary file: ");
        log_err(m_path.c_str());
        log_err(".");
        log_err_end();
    }
}


std::size_t temp_file::tell()
{
    const std::streampos offset = m_stream.tellp();
    if (offset == -1)
        throw std::system_error(
            errno, std::system_category(),
            "Error in temporary file '" + path() + "'");
    return std::size_t(offset);
}


void temp_file::write_line(stringlet line)
{
    m_stream << line << '\n';
}


void temp_file::close_output()
{
    if (!m_stream.flush())
        throw std::system_error(
            errno, std::system_category(),
            "Error writing to temporary file '" + path() + "'");
    m_stream.close();
}


std::string get_default_tmp()
{
#if defined(_WIN32)
    char path_buf[MAX_PATH];
    const int ret = GetTempPath(MAX_PATH, path_buf);
    if (ret == 0 || std::size_t(ret) > sizeof(path_buf))
        // TODO: Get more error information from GetLastError().
        throw std::runtime_error(
            "Could not determine default temporary directory.");
    return path_buf;
#else
    // POSIX says to try these environment variables, in this order:
    for (auto var: {"TMPDIR", "TMP", "TEMPDIR", "TEMP"})
    {
        const char *value = std::getenv(var);
        if (value && *value) return value;
    }
    // No environment variables set.  Default to /tmp.
    return "/tmp";
#endif
}
