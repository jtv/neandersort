/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

/** Neandersort main program.
 *
 * Sorting is done in a two-stage process:
 * 1. "Split" stage: pre-sort chunks of the text, writes them to temp files.
 * 2. "Merge" stage: combines those chunks into sorted output.
 *
 * The two barely share any code, so this is a helpful division for starting to
 * look at the code.  Have a look at split() and merge(), and their respective
 * call trees.
 *
 * Multiple chunks can be written consecutively to a single temporary file.
 * Some systems are restrictive about how many temporary files we can create.
 * Once written in the "split" stage, the temporary files get mapped into
 * read-only memory for the "merge" stage.
 */
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>

#include "chunk.h"
#include "except.h"
#include "log.h"
#include "input_sources.h"
#include "merge.h"
#include "output_stream.h"
#include "split.h"
#include "temp_file_pool.h"
#include "threading.h"


namespace boost_opts = boost::program_options;


boost_opts::variables_map parse_options(int argc, char **argv)
{
    const int default_jobs = std::max(1, num_cpus() - 1);
    const std::string default_temp_dir = get_default_tmp();

    boost_opts::options_description desc("Primitive text sorting utility.");
    desc.add_options()
        ("help,h", "Print usage information.")
        ("unique,u", "Remove duplicate lines.")
        (
            "input,i",
            boost_opts::value<std::vector<std::string>>(),
            "Input file(s), or '-' (the default) for standard input."
        )
        (
            "output,o",
            boost_opts::value<std::string>()->default_value("-"),
            "Output file, or '-' (the default) for standard output."
        )
        (
            "temporary-directory,T",
            boost_opts::value<std::string>()->default_value(default_temp_dir),
            "Where to create temporary files while sorting."
        )
        (
            "buffer-size,S",
            boost_opts::value<float>()->default_value(20.0),
            "Size of each sort buffer, in megabytes."
        )
        (
            "parallel,j",
            boost_opts::value<int>()->default_value(default_jobs),
            "Run this number of worker threads "
            "(in addition to the main thread)."
        )
        (
            "ungzip,z",
            "Decompress input with gzip, even if name doesn't end in .gz."
        )
        (
            "gzip,Z",
            "Compress output with gzip, even if name doesn't end in .gz."
        )
        ("verbose,v", "Print what's going on to standard error output.")
        ;
    boost_opts::positional_options_description args;
    args.add("input", -1);

    boost_opts::variables_map vars;
    auto parser = boost_opts::command_line_parser(argc, argv);
    boost_opts::store(parser.options(desc).positional(args).run(), vars);
    boost_opts::notify(vars);

    if (vars.count("help") > 0)
    {
        std::cout << desc << std::endl;
        exit(0);
    }

    return std::move(vars);
}


void sort(const boost_opts::variables_map &args)
{
    const bool verbose = (args.count("verbose") > 0);
    const bool unique = (args.count("unique") > 0);
    const std::string temp_dir = args["temporary-directory"].as<std::string>();
    // Use decimal megabytes.  Allocating exact powers of two can perform
    // badly because of allocation overheads.
    const std::size_t bufsize = std::size_t(
        args["buffer-size"].as<float>() * 1'000'000);
    const unsigned int jobs = args["parallel"].as<int>();

    if (verbose)
        std::cerr
            << "Creating up to " << jobs << " worker threads." << std::endl
            << "Creating temporary files in " << temp_dir << "." << std::endl
            << "Using buffers of " << bufsize << " bytes each." << std::endl
            ;
    if (bufsize < 2) throw command_line_error("Buffer size too small.");

    input_sources inputs(
        (args.count("input") > 0) ?
            args["input"].as<std::vector<std::string>>() :
            std::vector<std::string>(1, "-"),
        (args.count("ungzip") > 0));

    output_stream output(
        args["output"].as<std::string>(),
        (args.count("gzip") > 0));

    temp_file_pool temp_files(temp_dir);
    auto chunks = split(inputs, temp_files, unique, bufsize, jobs);
    if (verbose)
        std::cerr
            << "End of input phase." << std::endl
            << "Merging " << chunks.size() << " chunks from "
            << temp_files.size() << " temporary file(s)."
            << std::endl
            ;
// TODO: Any chance of pre-merging some chunks first?
    auto mapped_files = map_chunks(chunks);
    merge(chunks, output, unique);
    if (verbose)
        std::cerr << "Done sorting.  Flushing output." << std::endl;
    output.close();
}


int main(int argc, char *argv[])
{
    try
    {
        auto opts = parse_options(argc, argv);
        sort(opts);
    }
    catch (const std::exception &error)
    {
        log_err(error.what());
        log_err_end();
        exit(1);
    }
}
