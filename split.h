/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

class hunk;
class input_sources;
class temp_file_pool;


/** Perform the first, "split" part of the sort algorithm.
 *
 * Processes all input and splits it into chunks, each of which is sorted and
 * standardized on Unix-style line endings.
 *
 * The "merge" part of the algorithm then splices these chunks together to
 * produce sorted output.
 */
std::vector<std::unique_ptr<chunk>> split(
    input_sources &inputs,
    temp_file_pool &temp_files,
    bool unique,
    std::size_t bufsize,
    int jobs);
