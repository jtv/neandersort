/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <atomic>
#include <exception>
#include <functional>

#include <boost/thread/locks.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "thread_pool.h"


thread_pool::thread_pool(int capacity, std::function<void ()> on_error) :
    m_mutex(),
    m_threads(),
    m_quiescing(false),
    m_abort(false),
    m_error(),
    m_on_error(on_error),
    m_owner(threading::this_thread::get_id()),
    m_idle(0)
{
    m_threads.reserve(capacity);
}


thread_pool::~thread_pool()
{
    if (!m_quiescing.load())
    {
        // Exiting abnormally.  Threads in the pool may still be running.
        request_abort();
        try { join(); } catch (const std::exception &) {}
    }
    else
    {
        // Normal completion.  If any threads are still running, there's a bug.
        // and the threading library will call terminate().
    }
}


std::size_t thread_pool::size() const noexcept
{
    lock_guard lock(m_mutex);
    return m_threads.size();
}


void thread_pool::sleep()
{
    unique_lock lock(m_mutex);
    check_error(lock);
    m_idle++;
    m_wake_up.wait(lock);
    m_idle--;
    check_error(lock);
}


void thread_pool::sleep(
    std::function<bool()> predicate,
    std::function<void()> on_wakeup)
{
    unique_lock lock(m_mutex);
    check_error(lock);
    m_idle++;
    // Don't bother restoring m_idle on failure; failure will be total anyway.
    m_wake_up.wait(lock, predicate);
    m_idle--;
    check_error(lock);
    on_wakeup();
}


void thread_pool::wake_up_thread()
{
    m_wake_up.notify_one();
}


void thread_pool::request_quiesce()
{
    m_quiescing.store(true, std::memory_order_relaxed);
    m_wake_up.notify_all();
}


void thread_pool::request_abort()
{
    m_abort.store(true, std::memory_order_relaxed);
    request_quiesce();
    // Dammit.  This is Boost-specific.  No std::thread::interrupt in C++11. :(
    for (auto &thread: m_threads) thread.interrupt();
}

void thread_pool::join()
{
    assert(m_quiescing.load());
    for (auto &thread: m_threads) if (thread.joinable()) thread.join();
    check_error();
}


void thread_pool::register_error()
{
    lock_guard lock(m_mutex);
    if (m_error == nullptr) m_error = std::current_exception();
    m_on_error();
}


void thread_pool::check_error() const
{
    if (threading::this_thread::get_id() == m_owner)
    {
        lock_guard lock(m_mutex);
        if (m_error != nullptr) std::rethrow_exception(m_error);
    }
    else
    {
        if (is_aborting()) throw stop_threads();
    }
}


void thread_pool::check_error(const unique_lock &lock) const
{
    assert(lock.owns_lock());
    if (threading::this_thread::get_id() == m_owner)
    {
        if (m_error != nullptr) std::rethrow_exception(m_error);
    }
    else
    {
        if (is_aborting()) throw stop_threads();
    }
}


int thread_pool::idle_threads() const noexcept
{
    lock_guard lock(m_mutex);
    return m_idle;
}
