/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "threading.h"


class chunk;


/// Pool of sorted chunks of lines.
class chunk_pool
{
public:
    /// Add a chunk to the pool.  Protect this with an external lock!
    void add_result(std::unique_ptr<chunk> &&, const lock_guard &);

private:
    std::vector<std::unique_ptr<chunk>> m_chunks;
};
