/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

class stringlet;


/** Temporary file, to be deleted on destruction.
 */
class temp_file
{
public:
    explicit temp_file(const std::string &temp_dir);

    temp_file(const temp_file &) =delete;
    temp_file(temp_file &&) =delete;
    temp_file &operator=(const temp_file &) =delete;
    temp_file &operator=(temp_file &&) =delete;

    ~temp_file();
    
    const std::string &path() const noexcept { return m_path; }

    void write_line(stringlet);

    std::size_t tell();

    void close_output();

private:
    std::string m_path;
    std::ofstream m_stream;
};


/// Return default location for temporary files.
std::string get_default_tmp();
