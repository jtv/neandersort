/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <atomic>
#include <cassert>
#include <fstream>
#include <memory>
#include <vector>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "chunk.h"
#include "lines_buffer.h"
#include "merge.h"
#include "output_stream.h"
#include "threading.h"
#include "thread_pool.h"


namespace
{
/// Skip any occurrances of "line" in "chnk".
void skip_line(chunk &chnk, stringlet prev_line)
{
    while (chnk.has_line() && chnk.line() == prev_line) chnk.next_line();
}


/** Trivial merge: write a single chunk straight to output.
 *
 * No need to eliminate duplicates.  If we don't want those, they never went
 * into the chunk in the first place.
 */
void trivial_merge(chunk &chnk, lines_buffer &output)
{
    do
    {
        // Save up a batch of lines, then send them off to the output thread.
        std::vector<stringlet> lines;
        lines.reserve(5'000);
        while (chnk.has_line() && lines.size() < lines.capacity())
        {
            lines.push_back(chnk.line());
            chnk.next_line();
        }
        if (!lines.empty()) output.push(std::move(lines));
    } while (chnk.has_line());
}


// TODO: Special-case near-trivial case of merging exactly 2 chunks?


/** Heap comparison for chunks, based on their first remaining line.
 *
 * Us this for maintaining a heap of unique_ptr<heap>.  It sorts in inverse
 * lexicographical order, beacuse the "top" of the heap is its "greatest"
 * element.  We want the lexicographically "least" line at the top.
 */
inline bool chunk_line_cmp(
    const std::unique_ptr<chunk> &lhs,
    const std::unique_ptr<chunk> &rhs) noexcept
{
    return rhs->line() < lhs->line();
}


/** Arrange a vector of chunks into a heap.
 *
 * A heap is a data structure which supports O(log log n) extraction of the
 * top element.
 */
void make_heap(std::vector<std::unique_ptr<chunk>> &chunks)
{
    assert(!chunks.empty());
    // Eliminate empty chunks.
    chunks.erase(
        std::partition(
            chunks.begin(), chunks.end(),
            [](const std::unique_ptr<chunk> &c){return c->has_line();}),
        chunks.end());

    // Order chunks into a heap.
    std::make_heap(chunks.begin(), chunks.end(), chunk_line_cmp);
}


/** Merge lines from heap of chunks, send them to the output thread.
 *
 * This function is templatized on the "unique" parameter, so the compiler can
 * specialize the inner loop.
 */
template<bool unique> void merge_lines(
    std::vector<std::unique_ptr<chunk>> &heap, lines_buffer &output)
{
    /* Keeps a heap of chunks, ordered by their first remaining line.  The main
     * loop takes the "top" chunk out of the heap, writes out its first line,
     * then moves it along to its next line, and reinserts the chunk into the
     * heap based on the ordering of that line.
     */

    // Track the previous line, so we can eliminate duplicates.
    stringlet prev_line;
    bool have_prev_line = false;
    const auto heap_begin = heap.begin();
    auto heap_end = heap.end();
    while (std::distance(heap_begin, heap_end) > 1)
    {
        std::vector<stringlet> lines;
        lines.reserve(2'000);
        while (std::distance(heap_begin, heap_end) > 1 && lines.size() < lines.capacity())
        {
            // Move stream with the lexicographically "least" line to the back.
            std::pop_heap(heap_begin, heap_end, chunk_line_cmp);
            auto &head = *(heap_end - 1);
            auto head_line = head->line();
            if (!unique)
            {
                lines.push_back(head_line);
            }
            else if (!have_prev_line || head_line != prev_line)
            {
                lines.push_back(head_line);
                prev_line = head_line;
                have_prev_line = true;
            }
            else
            {
                // Duplicate line.  Ignore.
            }

            // Move this stream ahead to its next line.
            head->next_line();
            if (head->has_line())
            {
                // This stream's not done yet.  Shuffle back into the heap.
                std::push_heap(heap_begin, heap_end, chunk_line_cmp);
            }
            else
            {
                // This stream is done.  Drop it from the heap.  Keep it in the
                // vector though, because prev_line may still be pointing to it.
                --heap_end;
            }
        }
        if (!lines.empty()) output.push(std::move(lines));
    }
    // Only one stream left?  Switch to faster dump.
    if (heap_end > heap_begin)
    {
        if (unique && have_prev_line) skip_line(**heap_begin, prev_line);
        trivial_merge(**heap_begin, output);
    }
}


/// Main loop for output thread.
void write_lines(
    thread_pool *threads,
    lines_buffer *queue,
    output_stream *output)
{
    try
    {
        while (true)
        {
            const auto lines = queue->pop();
            if (lines.empty() && threads->is_quiescing())
                throw stop_threads();
            for (auto line: lines)
                output->write_line(line);
        }
    }
    catch (const stop_threads &)
    {
    }
    catch (const std::exception &)
    {
        threads->register_error();
    }
}

} // namespace


// TODO: Async I/O?  Why should compression wait for ofstream writes?

void merge(
    std::vector<std::unique_ptr<chunk>> &chunks,
    output_stream &output,
    bool unique)
{
    if (chunks.empty()) return;

    thread_pool threads(1);
    lines_buffer outgoing_queue(threads);
    if (!threads.allocate(write_lines, &threads, &outgoing_queue, &output))
        throw std::logic_error("Internal error: Could not allocate thread.");
    make_heap(chunks);
    if (unique) merge_lines<true>(chunks, outgoing_queue);
    else merge_lines<false>(chunks, outgoing_queue);
    threads.request_quiesce();
    threads.join();
}
