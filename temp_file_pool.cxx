/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <atomic>
#include <cstddef>
#include <fstream>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "temp_file_pool.h"
#include "thread_pool.h"


temp_file_pool::temp_file_pool(const std::string &temp_dir) :
    m_temp_dir(temp_dir),
    m_files(),
    m_available()
{
}


int temp_file_pool::grab()
{
    lock_guard lock(m_mutex);
    assert(m_available.size() == m_files.size());
    const auto avail_file = std::find(
        m_available.begin(), m_available.end(), true);
    const std::size_t id = avail_file - m_available.begin();
    if (avail_file == m_available.end())
    {
        m_files.emplace_back(new temp_file(m_temp_dir));
        m_available.emplace_back(false);
    }
    else
    {
        assert(m_available[id]);
        m_available[id] = false;
    }
    assert(m_available.size() == m_files.size());
    assert(id < m_files.size());
    assert(!m_available[id]);
    return id;
}


void temp_file_pool::release(int id) noexcept
{
    lock_guard lock(m_mutex);
    assert(id >= 0);
    assert(std::size_t(id) < m_files.size());
    assert(m_available.size() == m_files.size());
    assert(!m_available[id]);
    m_available[id] = true;
}


temp_file &temp_file_pool::get(int id) noexcept
{
    lock_guard lock(m_mutex);
    assert(id >= 0);
    assert(std::size_t(id) < m_files.size());
    assert(m_available.size() == m_files.size());
    assert(!m_available[id]);
    return *m_files[id];
}


const temp_file &temp_file_pool::get(int id) const noexcept
{
    lock_guard lock(m_mutex);
    assert(id >= 0);
    assert(std::size_t(id) < m_files.size());
    assert(m_available.size() == m_files.size());
    assert(!m_available[id]);
    return *m_files[id];
}


std::size_t temp_file_pool::size() const noexcept
{
    lock_guard lock(m_mutex);
    return m_files.size();
}


void temp_file_pool::flush()
{
    for (auto &file: m_files) file->close_output();
}
