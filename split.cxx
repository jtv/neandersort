/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <atomic>
#include <cassert>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/lock_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "buffer_pool.h"
#include "chunk.h"
#include "chunk_pool.h"
#include "except.h"
#include "index.h"
#include "input_sources.h"
#include "temp_file_pool.h"
#include "thread_pool.h"


namespace
{
/** Find an incomplete trailing line at the end of a buffer.
 *
 * The line string is passed in from the outside to save allocations.
 * Doing it this way makes it reuse existing storage.
 */
void find_incomplete_line(const std::vector<char> &buffer, std::string &line)
{
    if (buffer.empty())
    {
        line.clear();
    }
    else
    {
        const auto newline = find_last_newline(buffer);
        line.assign(newline + 1, buffer.end());
    }
}


/// Copy trailing incomplete line from last buffer into new one.
void move_incomplete_line(
    const std::string &incomplete_line, std::vector<char> &new_buffer)
{
    assert(!incomplete_line.empty());
    assert(new_buffer.empty());
    new_buffer.resize(incomplete_line.size());
    std::memcpy(
        &new_buffer[0], incomplete_line.c_str(), incomplete_line.size());
}


/** Write lines from buffer into output, in lexicographical order.
 *
 * The unique option is a template parameter, to allow the compiler to
 * eliminate some run-time code.
 */
template<bool unique> inline void store(
    const std::vector<stringlet> &index,
    chunk *store)
{
    stringlet prev_line;
    bool have_prev_line = false;
    for (auto line: index)
    {
        assert(line.data() != nullptr);
        if (!unique)
        {
            store->store_line(line);
        }
        else if (!have_prev_line || line != prev_line)
        {
            store->store_line(line);
            prev_line = line;
            have_prev_line = true;
        }
        else
        {
            // Duplicate line.  Ignore.
        }
    }
}


/// Sort a buffer of data into a chunk.
std::unique_ptr<chunk> sort_buffer(
    const std::vector<char> &buffer,
    temp_file_pool &temp_files,
    bool unique)
{
    auto index = index_buffer(buffer);
    // Sort the index.  The actual strings are still scattered around buffer.
    std::sort(index.begin(), index.end());
    const int file_id = temp_files.grab();
    temp_file &file = temp_files.get(file_id);
    auto result = std::make_unique<chunk>(file);
    if (unique) store<true>(index, result.get());
    else store<false>(index, result.get());
    result->complete_store();
    temp_files.release(file_id);
    return std::move(result);
}


/** Read a buffer's worth of data from given input stream(s).
 *
 * Appends to buffer, but only up to its current capacity.
 */
void read_buffer(input_sources &inputs, std::vector<char> &buffer)
{
    assert(buffer.capacity() > 1);
    assert(buffer.size() < buffer.capacity());
    if (inputs.empty()) return;
    // Leave one byte free for adding a newline at the end of a file.
    const std::size_t bufsize = buffer.capacity() - 1;
    // If the buffer is nearly full, don't try to fill it up further.  Send it
    // off for processing.
    const std::size_t headroom = std::min(bufsize / 4, std::size_t(100'000));
    while ((buffer.size() + headroom) < bufsize)
    {
        input_stream &stream = inputs.get();
        const std::size_t bytes_already_used = buffer.size();
        // Upsize to make room for additional bytes.
        buffer.resize(bufsize);
        const std::size_t bytes_read = stream.read(
            &buffer[bytes_already_used],
            bufsize - bytes_already_used);
        // Downsize to the actual number of bytes we have.
        buffer.resize(bytes_already_used + bytes_read);
        if (stream.eof())
        {
            if (bytes_read > 0 && buffer.back() != '\n')
                // No EOL at EOF.  Add one.
                buffer.push_back('\n');
            // Try to continue filling the same buffer from the next file,
            // if there is one.  Otherwise, we're done.
            if (!inputs.next()) return;
        }
    }
}


/// Worker thread's main loop: wait for full buffers and process them.
void consume_buffers(
    buffer_pool *buffers, thread_pool *threads, chunk_pool *chunks,
    temp_file_pool *temp_files, bool unique)
{
    try
    {
        /* Lock to protect access to the chunks pool.  It'd be nice to have this
         * embedded in chunks_pool, but it sabotages the class' move semantics.
         */
        static threading::mutex chunks_pool_lock;

        while (!threads->is_quiescing())
        {
            const int buffer_id = buffers->grab_full_buffer();
            if (buffer_id == buffer_pool::no_buffer)
            {
                threads->sleep();
            }
            else
            {
                auto &buffer = buffers->get(buffer_id);
                assert(!buffer.empty());
                auto chunk = sort_buffer(buffer, *temp_files, unique);
                buffers->deallocate(buffer_id);
                lock_guard lock(chunks_pool_lock);
                chunks->add_result(std::move(chunk), lock);
            }
        }
    }
    catch (const stop_threads &)
    {
        // Something else failed.  No need to do anything, just exit cleanly.
    }
    catch (const std::exception &)
    {
        threads->register_error();
    }
}
} // namespace


chunk_pool split(
    input_sources &inputs, temp_file_pool &temp_files, bool unique,
    std::size_t bufsize, int jobs)
{
    chunk_pool chunks;
    buffer_pool buffers(jobs * 2, unique);
    thread_pool threads(jobs, [&buffers](){buffers.abort();});

    bool have_data;
    std::string incomplete_line;
    do
    {
        const int buffer_id = buffers.allocate(bufsize, threads);
        if (buffer_id == buffer_pool::no_buffer) break;
        std::vector<char> &buffer = buffers.get(buffer_id);
        if (!incomplete_line.empty())
            move_incomplete_line(incomplete_line, buffer);
        read_buffer(inputs, buffer);
        find_incomplete_line(buffer, incomplete_line);
        have_data = !buffer.empty();
        if (have_data)
        {
            buffers.queue_full_buffer(buffer_id);
            bool new_thread = (threads.idle_threads() == 0);
            if (new_thread)
                new_thread = threads.allocate(
                    consume_buffers,
                    &buffers,
                    &threads,
                    &chunks,
                    &temp_files,
                    unique);
            if (!new_thread) threads.wake_up_thread();
        }
        else
        {
            buffers.deallocate(buffer_id);
        }
        threads.check_error();
    } while ((have_data || !incomplete_line.empty()));

    /* First wait until no more jobs need to start processing, then tell the
     * thread pool to wrap up once the running jobs are complete.
     */
    buffers.wait_none_full(threads);
    threads.request_quiesce();
    threads.join();

    temp_files.flush();
    return std::move(chunks);
}
