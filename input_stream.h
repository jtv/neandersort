/*
    Copyright 2015 Jeroen Vermeulen.

    This file is part of Neandersort.

    Neandersort is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Neandersort is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Neandersort.  If not, see <http://www.gnu.org/licenses/>.
*/

/** Input stream.
 *
 * Wraps either a file, possibly with transparent decompression, or stdin.
 */
class input_stream
{
public:
    input_stream() =default;

    input_stream(const input_stream &) =delete;
    input_stream &operator=(const input_stream &) =delete;

    ~input_stream();

    const std::string &path() const noexcept
    {
        assert(!m_path.empty());
        return m_path;
    }

    void close();

    void open(const std::string &path, bool ungzip=false);

    /// Read up to the given number of bytes into buffer.  Return bytes read.
    std::size_t read(char buffer[], std::size_t bytes);

    bool eof() const noexcept;

private:
    boost::iostreams::filtering_istream m_stream;
    std::string m_path;
    /// Owning handle on input file, if any.  Only used for cleanup.
    std::ifstream m_owned_file;
};
